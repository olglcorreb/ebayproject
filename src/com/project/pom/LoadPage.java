package com.project.pom;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


public class LoadPage extends Base{
	
	//Locators
	By mainPageLocator = By.id("gh");
	By searchBoxLocator = By.id("gh-ac");
	By searchBtnLocator = By.id("gh-btn");
	By brandLocator = By.cssSelector("input[aria-label=\"PUMA\"]");
	By sizeLocator = By.cssSelector("input[aria-label=\"10\"]");
	By resultsLocator =  By.xpath("//h1[@class='srp-controls__count-heading']//span[@class='BOLD']");
	By orderLocator =  By.cssSelector("#w9 .srp-controls__control--legacy");
	By lowpriceLocator = By.cssSelector(".btn:nth-child(4)");
	By priceLocator = By.cssSelector("#srp-river-results-listing1 .s-item__price");
	

	public LoadPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public String filterPagebyBrand() throws InterruptedException {
		Thread.sleep(2000);
		if(isDisplayed(mainPageLocator)) {
			printLog("Successfull Load.");
			type("shoes",searchBoxLocator);
			click(searchBtnLocator);
			printLog("Loock for Shoes.");
			click(brandLocator);
			printLog("PUMA has been selected.");
			String totalResultsBefore = getText(resultsLocator);
			printLog("Initial TotalResults -- "+totalResultsBefore);
			return totalResultsBefore;
		}
		else {
			printLog("Ebay Page was not found");
		}
		return null;
	}
	
	public String filterPagebySize() throws InterruptedException {
		click(sizeLocator);
		String totalResultsAfter = getText(resultsLocator);
		printLog("TotalResults Updated -- "+totalResultsAfter);
		return totalResultsAfter;
	}
	
	public Hashtable<String, Float> itemsByOrder(String order) throws Exception {
		printLog("\n --- Items ordered by "+order+" --- ");
		Hashtable<String,Float> itemsbyOrder = getItems(true);
		return (itemsbyOrder );
	}
	
	public void orderByLowPrice() throws Exception {
		Actions builder = createAction();
		WebElement element = findElement(orderLocator);
		builder.moveToElement(element).build().perform();
		printLog("\n Order bt has been selected. \n ");
		Thread.sleep(2000);
		click(lowpriceLocator);
		printLog("\n Low Price has been selected.\n ");
	}
	
	public void orderByName() throws Exception {
		Hashtable<String, Float> itemsByName = getItems(false);
		Vector<String> keysfromLowp = new Vector<String>(itemsByName.keySet());
		Collections.sort(keysfromLowp);
		Iterator<String> it = keysfromLowp.iterator();
		printLog("\n\n -- Items Ordered By Name in ascendant mode. -- \n");
		while (it.hasNext()) {
			String itemfromLowp =  (String)it.next();
			printLog("\n "+itemfromLowp + "\n -- " + itemsByName.get(itemfromLowp));
		}
		takeEvidence("orderedItems");
	}
	
	public void orderByPrice() throws Exception {
		Hashtable<String, Float> itemsByPrice = getItems(false);
		String[] keysfromLowp = (String[]) itemsByPrice.keySet().toArray(new String[0]);
		Float[] valuesfromLowp = (Float[]) itemsByPrice.values().toArray(new Float[0]);
		printLog("\n\n -- Items Ordered By Price in descendant mode. -- \n");	
		for (int x = 0; x < valuesfromLowp.length; x++) {
			for (int i = 0; i < valuesfromLowp.length-x-1; i++) {
				if(valuesfromLowp[i] < valuesfromLowp[i+1]){
					//Ordering values
		            float tmpv = valuesfromLowp[i+1];
		            valuesfromLowp[i+1] = valuesfromLowp[i];
		            valuesfromLowp[i] = tmpv;
		            //Ordering keys
		            String tmpk = keysfromLowp[i+1];
		            keysfromLowp[i+1] = keysfromLowp[i];
		            keysfromLowp[i] = tmpk;
				}
			}
		}
		for (int j=0; j<valuesfromLowp.length; j++) {
			printLog("\n "+ keysfromLowp[j]+ "\n -- " +valuesfromLowp[j]);
		}
	}
	
	public boolean checkLowPrice() throws Exception {
		float[] valuesfromLowp = getPricestoCheck();
		for (int j=0; j<valuesfromLowp.length-1; j++) {
			if (valuesfromLowp[j]>valuesfromLowp[j+1]) {
				return false;
			}
		}
		return true;
	}
	
}
