package com.project.pom;

import java.io.File;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;

public class Base {
	private WebDriver driver;
	
	public Base(WebDriver driver){
		this.driver = driver;
	}
	
	public WebDriver chromeDriverConnection() {
		System.setProperty("webdriver.chrome.driver", "./resources/drivers/chromedriver");
		driver = new ChromeDriver();
		return driver;
	}
	
	public WebElement findElement(By locator ) {
		return driver.findElement(locator);
	}
	
	public List<WebElement> findElements(By locator){
		return driver.findElements(locator);
	}
	
	public String getText(WebElement element) {
		return element.getText();
	}
	
	public String getText(By locator) {
		return driver.findElement(locator).getText();
	}
	
	public void type(String inputText, By locator) {
		driver.findElement(locator).sendKeys(inputText);
	}
	
	public void click(By locator) throws InterruptedException {
		driver.findElement(locator).click();
		Thread.sleep(2000);
	}
	
	public Boolean isDisplayed(By locator) {
		try {
			return driver.findElement(locator).isDisplayed(); 
		}catch (org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
	} 
	
	public void visit(String url) {
		driver.manage().window().maximize();
		driver.get(url);
	}
	
	public void printLog(String message) {
		System.out.println("\n----------------- "+message);
	}
	
	public Hashtable<String, Float> getItems(boolean print) throws Exception {
		Hashtable<String, Float> items = new Hashtable<String,Float>();
		for (int i = 1; i<6; i++) {
			String indexprice = "#srp-river-results-listing"+i+" .s-item__price";
			String indexname = "#srp-river-results-listing"+i+" .s-item__title";
			String indexshipping = "#srp-river-results-listing"+i+" .s-item__shipping";
			String key = getText(By.cssSelector(indexname));
			String value = getText(By.cssSelector(indexprice));
			String shipping = getText(By.cssSelector(indexshipping));
			String testextractprice = getStringPrice(value);
			String testextractshipping = getStringPrice(shipping);
			float totalPrice = extractPrice(testextractprice)+extractPrice(testextractshipping);
			items.put(key, totalPrice);
			if (print == true) {
				printItems(key, value, shipping, totalPrice);
			}
		}
		return items;
	}
	
	public void printItems(String key, String value, String shipping, float totalPrice ) {
		printLog("\n Item: "+key+" \n-- Price: "+value+" \n-- Shipping: "+shipping+"\n-- TotalPriceItem: "+totalPrice);
	}
	
	public float[] getPricestoCheck() throws Exception {
		float[] itemsPrice = new float[5];
		for (int i = 1; i<6; i++) {
			String indexprice = "#srp-river-results-listing"+i+" .s-item__price";
			String indexshipping = "#srp-river-results-listing"+i+" .s-item__shipping";
			String value = getText(By.cssSelector(indexprice));
			String shipping = getText(By.cssSelector(indexshipping));
			String testextractprice = getStringPrice(value);
			String testextractshipping = getStringPrice(shipping);
			float totalPrice = extractPrice(testextractprice)+extractPrice(testextractshipping);
			int j=i-1;
			itemsPrice[j]=totalPrice;
		}
		return itemsPrice;
	}
	
	public Actions createAction(){
		return (new Actions(driver));
	}
	
	public void takeEvidence(String event) throws Exception{
		File shot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileHandler.copy(shot, new File("./screenshots/"+event+"-"+System.currentTimeMillis()+".jpg"));
		printLog("\nThe Screenshot has been taken. \n");
	}
	
	public static String getStringPrice(String cadena) throws Exception {
		Pattern patron = Pattern.compile("(\\$[0-9 ]+([\\.,][0-9]+)?)");
		Matcher encaja = patron.matcher(cadena);
		if (encaja.find() != false) {
			return encaja.group();	
		}
		else {
			return "0";
		}
	}
	
	public static float extractPrice(String svalue) {
		svalue = svalue.replace(" ","");
		svalue = svalue.replaceAll("(\\$+([\\s])?)", "");
		return Float.parseFloat(svalue);
	}
}
