package com.project.pom;

import org.testng.annotations.Test;
import org.testng.Assert;
import java.util.Hashtable;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class LoadTest {
	
	private WebDriver driver;
	LoadPage loadPage;
	
  @BeforeClass
  public void beforeClass() {
	  loadPage = new LoadPage(driver);
	  driver = loadPage.chromeDriverConnection();
	  loadPage.visit("https://www.ebay.com/");
  }

  @AfterClass
  public void afterClass() {
	  driver.quit();
  }
  
  @Test
  public void ebayTest() throws Exception {
	  String counterbyBrand = loadPage.filterPagebyBrand();
	  String counterbySize = loadPage.filterPagebySize();
	  Assert.assertNotEquals("\nThe quantity of elements should be different. ", counterbyBrand, counterbySize);
	  Hashtable<String,Float> itemsbybestresult = loadPage.itemsByOrder("BestResult");
	  loadPage.orderByLowPrice();
	  Hashtable<String,Float> itemsbylowprice = loadPage.itemsByOrder("LowPrice");
	  Assert.assertFalse(itemsbybestresult.equals(itemsbylowprice), "\n Different elements have been deployed");
	  boolean chckPrice = loadPage.checkLowPrice();
	  Assert.assertTrue(chckPrice,"\n The items are not ordered by lowPrice");
	  loadPage.orderByName();
	  loadPage.orderByPrice();
  }
}
